<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="my" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="kr" uri="myFirstTagLibrary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Testfragen Katalog</title>
	<style type="text/css">
		td {
				vertical-align:top;
				padding-right: 1em;
			}
	</style>
</head>
<body>
	<h1>Testgebiete ausw&auml;hlen</h1>
	<form action="quest.do" method="get">
		<hr />
		<table><tr>
		<kr:blocks catalog="${catalog}" tag="td" rows="8">
			<input type="checkbox" name="area" value="${area.name}"> ${area.name}<br />
		</kr:blocks>
		</tr></table>
<%--
				<c:set var="counter" value="0" />
				<c:set var="row_num" value="7" />
		<table>
		<tr>
 					<c:forEach var="area" items="${catalog}">
 						<c:if test="${counter%row_num==0}">
 			<td>
 						<c:set var="tag_closed" value="false" />
 						</c:if>
 				<input type="checkbox" name="area" value="${area.name}"> ${area.name}<br />
 						<c:set var="counter" value="${counter+1}" />
 						<c:if test="${counter%row_num==0}">
 			</td>
 							<c:set var="tag_closed" value="true" />
 						</c:if>
 					</c:forEach>
 					<c:if test="${!tag_closed}">
 			</td>
 					</c:if>
 		</tr>
 		</table>
 --%>
		<hr />
		<input type="submit" />
	</form>
</body>
</html>