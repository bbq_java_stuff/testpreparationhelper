package de.bbq.wcd.kupferrabe.models;

import java.util.ArrayList;
import java.util.List;

import de.bbq.wcd.kupferrabe.helpers.Parser;

public class QuestionsModel extends ArrayList<Object> {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//------------------------------------------------
// Konstruktore
	public QuestionsModel() {
		super();
	}

	public QuestionsModel( AreaModel area ) {
		addQs(area.getFiles());
	}

	public QuestionsModel( CatalogModel catalog ) {
		for(AreaModel area : catalog){
			addQs( area.getFiles() );
		}
	}

	public QuestionsModel( List<String> files, CatalogModel catalog ) {
		addQs(files);
	}

//------------------------------------------------
	private void addQs( List<String> files ){
		QuestionModel question = new QuestionModel();
		for( String fname : files){
			this.add( new QuestionModel(fname) );
		}
	}
}
