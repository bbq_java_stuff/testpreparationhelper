package de.bbq.wcd.kupferrabe.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.bbq.wcd.kupferrabe.helpers.Parser;

public class QuestionModel {
	//----- Static
	static String path;
	// wird beim App-Start mit dem aus Context bekannten RealPath
	// für den Verzeichnis mit XML's gesetzt
	// Das XML-Verzeichniss ist in context aus InitParameter bekannt
	public static void setPath( String path ) {
		QuestionModel.path = path;
	}
	
	
	//----- Private Atributte
	private Integer number;
	private String text;
	private String answerId;	// Antwort
	private String answerComment;		// Kommentar zum Antwort
	private Map<String,String> options;
	private List<String> order;
	
	//----- Konstruktoren
	public QuestionModel() {
		super();
	}

	public QuestionModel( QuestionModel q ) {
		this.number = q.number;
		this.text = q.text;
		this.answerId = q.answerId;
		this.answerComment = q.answerComment;
		this.options = q.options;
		this.order = q.order;
	}
	
	public QuestionModel( String fname ) {
		this( Parser.questionXML(path+"/"+fname) );
	}
	// --------------------------------------------
	
	public void setOption( String id, String text) {
		if( options==null ){
			options = new HashMap<String,String>();
		}
		options.put(id, text);
		
		if( order== null ){
			order = new ArrayList<String>();
		}
		order.add(id);
	}
	public String getOption(String id) {
		String text=null;
		if( options!=null ){
			text = options.get(id);
		} 
		return text;
	}
	public List<String> getOptionSortedIds() {
		if( options!= null ){
			List<String> list = new ArrayList<String>(options.keySet());
			Collections.sort(list);
			return list;
		}
		return null;
	}
	public List<String> getOptionOrderedIds() {
		if( options!= null ){
			return order;
		}
		return null;
	}

	
	////////////// Getter-Setter
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public String getText() {
		return this.text;
	}
	public void setText(String questhonText) {
		this.text = questhonText;
	}
	public String getAnswerId() {
		return answerId;
	}
	public void setAnswerId(String answerId) {
		this.answerId = answerId;
	}
	public String getAnswerComment() {
		return answerComment;
	}
	public void setAnswerComment(String answerComment) {
		this.answerComment = answerComment;
	}
	public Map<String, String> getOptions() {
		return options;
	}
	public void setOptions(Map<String, String> options) {
		this.options = options;
	}
	public List<String> getOrder() {
		return order;
	}
	public void setOrder(List<String> order) {
		this.order = order;
	}
}
