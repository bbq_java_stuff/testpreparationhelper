package de.bbq.wcd.kupferrabe.models;

import java.util.ArrayList;

public class AreaModel {
	private ArrayList<String> files = new ArrayList<String>();
	private String name;

	public void addFile( String fname ){
		files.add(fname);
	}

//----------- Getter und Setter ----------------
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public ArrayList<String> getFiles() {
		return files;
	}

	public void setFiles(ArrayList<String> files) {
		this.files = files;
	}
}
