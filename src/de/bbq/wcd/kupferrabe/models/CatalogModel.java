package de.bbq.wcd.kupferrabe.models;

import java.util.ArrayList;
import java.util.List;

import de.bbq.wcd.kupferrabe.helpers.Parser;

public class CatalogModel extends ArrayList<AreaModel> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> areaNames;
	
	public CatalogModel(){
		super();
	}

	public CatalogModel( String fname ){
		super( Parser.indexXML(fname) );
		areaNames = new ArrayList<String>();
		for( AreaModel area : this ){
			areaNames.add(area.getName());
		}
	}
	
	public void addArea( AreaModel area ){
		this.add(area);
	}

	// suchen ein Gebiet nach seinem Namen
	public AreaModel getArea( String name ){
		for(AreaModel area : this){
			if( area.getName().equals(name)){
				return area;
			}
		}
		return null;
	}

	// suchen ein Gebiet nach seinem Namen
	public AreaModel getAreaForFile( String fnameOut ){
		for(AreaModel area : this){
			for(String fname : area.getFiles()){
				if( fname.equals(fnameOut)){
					return area;
				}
			}
		}
		return null;
	}

	
	public List<String> getAreaNames(){
		return areaNames;
	}
}