package de.bbq.wcd.kupferrabe.helpers;

import java.io.IOException;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import de.bbq.wcd.kupferrabe.models.AreaModel;
import de.bbq.wcd.kupferrabe.models.CatalogModel;
import de.bbq.wcd.kupferrabe.models.QuestionModel;

public class Parser {
	
	static public void test(){
	}

	static public void setContext(){
	}

	// Katalog befindet sich in einer XML-Datei
	// Die Datei wird hier geparst und das Ergebnise in einem Objekt
	// von KLasse Catalog zurueckgegeben, das eine Liste mit Gebieten enthält
	// ein Gebiet enthelt sein Name und die Dateinamen von weiteren XML-Dateien
	// die eigentlichen Fragen einthalten
	static public CatalogModel indexXML(String fname){
		CatalogModel catalog = null;
		AreaModel area = null;
		List<Element> elements = null;
		Document doc = null;

		try {
			doc = new SAXBuilder().build( fname );
		} catch (JDOMException | IOException e) {
			e.printStackTrace();
		}

		Element rootElement = doc.getRootElement();
		List<Element> areas = rootElement.getChildren( "gebiet" );
		catalog = new CatalogModel();

		for( Element areaElement : areas){
			area = new AreaModel();
			// Gebietname
			area.setName( areaElement.getChild("name").getText() );
			elements = areaElement.getChild("files").getChildren();
			for( Element element : elements){
				// Frage-Dateiname
				area.addFile(element.getText());
			}
			catalog.addArea(area);
		}

		return catalog;
	}
	
	// Jede Datei aus dem Katalog enhält eine Testfrage und Loesung
	// Eine Testfrage besteht aus einem Nummer, Fragetext, Loesung(Buchstabe),
	// Kommentar zur Loesung und Loesungsvarianten(in einer Liste)
	// Eine Loesungsvariante besteht aus einer Buchstabe und einem Text
	static public QuestionModel questionXML(String fname){
		QuestionModel question = new QuestionModel();
		List<Element> elements = null;
		Document doc = null;

		try {
			doc = new SAXBuilder().build( fname );
		} catch (JDOMException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Element rootElement = doc.getRootElement();
		
		question.setNumber( Integer.valueOf( rootElement.getChildText( "num" ) ) );
		question.setText( rootElement.getChildText( "frage" ) );
		question.setAnswerId( rootElement.getChild( "loesung" ).getChildText("num") );
		question.setAnswerComment( rootElement.getChild( "loesung" ).getChildText("comment") );
		
		elements = rootElement.getChildren("auswahl");
		for( Element choice : elements){
			question.setOption( choice.getChildText("num"), choice.getChildText("text"));
		}

		return question;
	}

//	public static void main(String[] args) {
//		//System.out.println( Arrays.toString( (new File("raw_data")).list() ) );
//		Catalog cat = indexXML("raw_data/index.xml");
//	}
}
