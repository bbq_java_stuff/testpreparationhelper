package de.bbq.wcd.kupferrabe.helpers;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import de.bbq.wcd.kupferrabe.models.CatalogModel;
import de.bbq.wcd.kupferrabe.models.QuestionModel;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent contextEvent) {
    	ServletContext context = contextEvent.getServletContext();
		String xmlDir = context.getInitParameter("XML_DIR");
		
		String xmlPath = context.getRealPath(xmlDir);
		context.setAttribute("XML_PATH", xmlPath);
		// den Path zur XML auch fuer Fragen zugaenglich machen... 
		QuestionModel.setPath(xmlPath);
		
		CatalogModel catalog = new CatalogModel(xmlPath+"/index.xml");
		context.setAttribute("catalog", catalog);
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }
	
}
