package de.bbq.wcd.kupferrabe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.bbq.wcd.kupferrabe.models.AreaModel;
import de.bbq.wcd.kupferrabe.models.CatalogModel;

/**
 * Servlet implementation class QuestionsController
 */
@WebServlet("/QuestionsController")
public class Quest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Quest() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doAction(request, response);
	}
	
	protected void doAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Map mit allen Parameter von der vorherige Seite bekommen
		Map<String,String[]> map = request.getParameterMap();
		
		// alle Parameter mit dem Namen "area" in ein Array speichern
		// (Gebietesnamen)
		List<String> names = new ArrayList<String>( java.util.Arrays.asList(map.get("area")) );
		
		// Geietsbnamen wetergeben
		request.setAttribute("area_names", names);
		
		// den gesamten Fragenkatalog nehmen
		ServletContext context = getServletContext();
		CatalogModel catalog = (CatalogModel) context.getAttribute("catalog");
		
		// Liste mit Gebitsobjekten erstellen (jeder mit eignenen Fragen)
		List<AreaModel> areas = new ArrayList<AreaModel>();
		for(String name : names){
			areas.add( catalog.getArea(name) );
		}
		// und als Parameter weitergeben
		request.setAttribute("areas", areas);
			
		// an View weiterleiten
		RequestDispatcher view = request.getRequestDispatcher("QuestView.jsp");
		view.forward(request, response);
	}

}
