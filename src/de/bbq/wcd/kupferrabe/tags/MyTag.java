package de.bbq.wcd.kupferrabe.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.SkipPageException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import de.bbq.wcd.kupferrabe.models.AreaModel;
import de.bbq.wcd.kupferrabe.models.CatalogModel;

public class MyTag extends SimpleTagSupport {

	private CatalogModel catalog;
	private String tag;
	private Integer rows;

	public void setCatalog(CatalogModel catalog){ this.catalog = catalog; }
	public void setTag(String tag){ this.tag = tag; }
	public void setRows(Integer rows){ this.rows = rows; }
	
	@Override
	public void doTag() throws JspException, IOException {
		JspWriter out = getJspContext().getOut();
		if(catalog != null){
			int counter=0;
			out.print("<"+tag+">");
			for (AreaModel area : catalog) {
				getJspContext().setAttribute("area", area);
				getJspBody().invoke(null);
				if( (++counter % rows)==0 && counter!=catalog.size() ){
					out.print("</"+tag+"><"+tag+">");
				}
			}
			out.print("</"+tag+">");
		} else{
			out.print("<td>&nbsp;</td>");
		}

	}
}
